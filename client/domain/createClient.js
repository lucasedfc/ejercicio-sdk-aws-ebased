//@ts-check
const { validateParams } = require("../helper/validateAge");
const { CreateClientValidation } = require("../schema/input/createClientValidation");
const { v4: uuidv4 } = require('uuid');
const { saveClient } = require('../service/saveClient');
const { emitClientCreated } = require("../service/emitClient");
const { ClientCreatedEvent } = require("../schema/event/emitClientValidation");


module.exports = async (commandPayload, commandMeta) => {

    commandPayload.id = uuidv4();    
    console.log(commandPayload);
    const data = new CreateClientValidation(commandPayload, commandMeta).get();  
    
    try {        
        validateParams(data);
    } catch (error) {
        return {status: 400, body: {message: error.message} };   
    }

    console.log({data});
    await saveClient(data);    
    await emitClientCreated(new ClientCreatedEvent(data, commandMeta));
    
    return {status: 200, body: 'Client Created' };   

}
